
package com.test.refactor.utils;

import java.util.ArrayList;

public class CalculatePrime {

    private static int[] primes;
    private static ArrayList<Integer> multiplesOfPrimeFactors;

    private CalculatePrime() {

    }

    public static int[] calculatePrimeArray(int n) {
        primes = new int[n];
        multiplesOfPrimeFactors = new ArrayList<>();
        firstPrime();
        generatePrimes();
        return primes;
    }

    private static void firstPrime() {
        primes[0] = 2;
        multiplesOfPrimeFactors.add(2);
    }

    private static void generatePrimes() {
        int index = 1;
        for (int prime = 3; index < primes.length; prime += 2) {
            if (isPrime(prime))
                primes[index++] = prime;
        }
    }

    private static boolean isPrime(int prime) {
        if (lastMultipleOfPrime(prime)) {
            multiplesOfPrimeFactors.add(prime);
            return false;
        }
        return notMultipleOfPrime(prime);
    }



}
