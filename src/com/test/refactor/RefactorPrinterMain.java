
package com.test.refactor;

import com.test.refactor.utils.CalculatePrime;
import com.test.refactor.utils.PrinterHelper;

public class RefactorPrinterMain {

    public static void main(String[] args) {
        final int PRIME_NUMBERS = 1000;
        int[] primes = CalculatePrime.calculatePrimeArray(PRIME_NUMBERS);

        final int ROWS_PER_PAGE = 50;
        final int COLUMNS_PER_PAGE = 4;
        PrinterHelper printerPrimes = new PrinterHelper(ROWS_PER_PAGE, COLUMNS_PER_PAGE);

        printerPrimes.printAll(primes);
    }
}
